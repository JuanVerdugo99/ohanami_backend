const {Router} = require('express');
const { check } = require('express-validator');

const { createUser, login, renewToken } = require('../controller/auth');
const { validateFields } = require('../middlewares/validate-fields');
const { validateJWT } = require('../middlewares/validate-jwt');

const router = Router();

// Create user
router.post('/new', [
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('email', 'El correo es obligatorio').not().isEmpty().isEmail(),
    check('password', 'La contraseña es obligatorio').not().isEmpty(),
    validateFields
], createUser);

router.post('/', [
    check('email', 'El email es obligatorio').not().isEmpty().isEmail(),
    check('password', 'La contraseña es obligatorio').not().isEmpty()
], login);

router.get('/renew', validateJWT, renewToken)

module.exports = router;