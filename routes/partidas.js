const {Router} = require('express');
const router = Router();

const Partida = require('../models/Partida');

router.get('/', async (req, res) => {
    await Partida.find().sort('-fecha').exec(function(err, post) {
        res.json({partidas: post});
    });
});

router.get('/:id', async (req, res) => {
    const partidas = await Partida.find({idUser: req.params.id}).sort('-fecha').exec(function(err, post) {
        res.json({partidas: post});
    });
});

router.get('/una/:id', async (req, res) => {
    const partidas = await Partida.findById(req.params.id);
    res.json({partidas: [partidas]});
});

router.post('/create', async (req, res) => {
    try {
        const partida = new Partida(req.body)

        await partida.save();

        res.json({
            ok: true,
            msg: 'Partida Creada',
            partida,
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hubo un problema interno'
        });
    }
});

router.delete('/:id', async (req, res) => {
    await Partida.findByIdAndDelete(req.params.id);
    res.json({ status: "Tutorado eliminado" });
});

module.exports = router;