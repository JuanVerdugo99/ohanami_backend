const express = require('express');
const cors = require('cors');
const path = require('path');
require('dotenv').config();

// DB config
require('./database/config').dbConnection();

// App de Express
const app = express();

app.use(express.json());
app.use(cors());

// Routes
app.use('/api/login', require('./routes/auth'));
app.use('/api/partida', require('./routes/partidas'));

app.listen( process.env.PORT, ( ) => {
    // if ( err ) throw new Error(err);
    console.log('Servidor corriendo en puerto', process.env.PORT );
});


