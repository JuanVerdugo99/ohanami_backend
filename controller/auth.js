const {response, request} = require('express')
const bcrypt = require('bcryptjs')

const User = require('../models/user');
const { generateJWT } = require('../helpers/jwt');

const createUser = async (req = request, res = response) => {

    const {email, password} = req.body;

    try {
        const existsEmail = await User.findOne({email: email});
        if( existsEmail ) {
            res.status(400).json({
                ok: false,
                msg: 'Ya existe este correo'
            });
        }
        const userDB = new User(req.body)

        // Encrypy Password
        const salt = bcrypt.genSaltSync();
        userDB.password = bcrypt.hashSync(password, salt);

        await userDB.save();

        // Generate JSON WEB TOKEN
        const token = await generateJWT(userDB.id);

        res.json({
            ok: true,
            msg: 'Usuario Creado',
            userDB,
            token
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hubo un problema interno'
        });
    }
}

const login = async (req = request, res = response) => {

    const {email, password} = req.body;

    try {
        const userDB = await User.findOne({email});

        if (!userDB) {
            return res.status(404).json({
                ok: false,
                msg: 'Email no encontrado'
            })
        }

        const validatePassword = bcrypt.compareSync(password, userDB.password);
        if (!validatePassword) {
            return res.status(400).json({
                ok: false,
                msg: 'La contraseña no es valida'
            })
        }

        const token = await generateJWT(userDB.id);

        return res.json({
            ok: true,
            msg: 'login',
            userDB,
            token
        });

    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Hubo un problema interno'
        })
    }
    
}

const renewToken = async (req = request, res = response) => {
    const uid = req.uid;

    const token = await generateJWT(uid);

    const userDB = await User.findById(uid);
    res.json({
        ok: true,
        msg: 'Token Actualizado',
        userDB,
        token
    })
}

module.exports = {
    createUser,
    login,
    renewToken
}