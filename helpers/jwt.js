const jwt = require('jsonwebtoken');

const generateJWT = (uid) => {

    return new Promise( (res, rej) => {

        const payload = {uid};

        jwt.sign(payload, process.env.JWT_KEY, {
            expiresIn: '48h'
        }, (err, token) => {
            if (err) {
                rej('No se pudo generar el token');
            } else {
                res(token);
            }
        });
    })
}

module.exports = {
    generateJWT
}