#Proyecto Ohanami 
<hr style="border:2px solid gray"> </hr>

Para correr el servidor del proyecto de ohanami en su computadora, lo primero que se debe de hacer es asegurarse de tener node y npm ya instalados en su dispositivo con **node -v** y **npm -v**

Abajo esta el link para descargarlo;
https://nodejs.org/es/

Una vez clonado el repositorio, se tiene que instalar las dependencias con el comando **npm i** O **npm install** dentro del repositorio.

Por ultimo, ingresar el comando **npm run start:dev** para tener el backend listo y poder usar la aplicación.

