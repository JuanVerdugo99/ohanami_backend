const {Schema, model} = require('mongoose');

const partidaSchema = new Schema({
    idUser: String,
    jugadores: [
        {
            jugador: String,
            puntuacion: Number,
        }
    ],
    totalCartas: {
        cartasAzules: Number,
        cartasVerdes: Number,
        cartasGrises: Number,
        cartasRosas: Number,
    },
    fecha: {
        type: Date,
        default: Date.now
    }
});

module.exports = model('Partida', partidaSchema)